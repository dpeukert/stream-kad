# Stream KAD

A script for OBS that updates text sources you choose with your kill, assist and death stats from CS:GO.

Only tested on Linux.

## How to run this

1. Copy the provided `gamestate_integration_stream_kad.cfg` file to `$YOUR_CSGO_FOLDER/csgo/cfg/gamestate_integration_stream_kad.cfg`
2. Add the `stream-kad.py` script in OBS (Tools > Scripts)
3. In the same window, configure your Steam ID and the sources you want to change the text of

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
