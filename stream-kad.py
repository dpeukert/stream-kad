#!/usr/bin/env python3

import http.server
import json
import obspython as obs
import threading
import time

# List of stats we want to track
data_types = [
	'kills',
	'assists',
	'deaths',
]

# Global variable that stores our HTTP server so that we can access it from the script_* functions
server = None

# Global variable that stores our main class so that we can access it from the script_* functions
stream_kad = None

# Modified HTTPServer class with customizations
class StreamKADServer(http.server.HTTPServer):
	# Add a prop for our custom handler to be called on incoming POST requests
	def serve_forever(self, post_handler):
		print('Starting server on port {}'.format(self.socket.getsockname()[1]))
		self.RequestHandlerClass.post_handler = post_handler
		http.server.HTTPServer.serve_forever(self)

	# Output messages when shutting down
	def server_close(self):
		print('Shutting down server')
		http.server.HTTPServer.server_close(self)
		print('Server shut down')

# Modified HTTPServer request handler class with customizations
class StreamKADRequestHandler(http.server.BaseHTTPRequestHandler):
	post_handler = None

	# Don't output any error messages for incoming requests
	def log_message(self, format, *args):
		pass

	# Call our handler with the data when we get a incoming POST request
	def do_POST(self):
		# Send an empty 200 response back to acknowledge the request
		self.send_response(200)
		self.end_headers()

		# Read the incoming data according to the Content-Length header
		length = self.headers['content-length']
		data = self.rfile.read(int(length))

		# Parse the JSON or use the fallback empty object
		if data and data != '':
			data = json.loads(data)
		else:
			data = {}

		# Send the data to our handler
		self.post_handler(data)

# Our main class, handles data validation & communication w/ OBS
class StreamKAD:
	# Steam ID of the user we want to track
	steam_id = None

	# Source names for all the stats we want to track
	sources = {}

	# Last values of all the stats we want to track, stored in order to not make unnecessary changes
	last_states = {}

	def __init__(self):
		global data_types

		for data_type in data_types:
			self.sources[data_type] = None
			self.last_states[data_type] = None

	# Fill the provided list properties with text source names
	def set_source_lists(self, props):
		sources = self.get_text_sources()

		for prop in props:
			obs.obs_property_list_clear(prop)
			obs.obs_property_list_add_string(prop, '', '')

			for source in sources:
				obs.obs_property_list_add_string(prop, source, source)

	# Get names of all text sources
	def get_text_sources(self):
		text_sources = []
		sources = obs.obs_enum_sources()

		for source in sources:
			# Filter out any non-text sources
			# TODO: check if non-Linux platforms use a different source type/ID
			if obs.obs_source_get_id(source) == 'text_ft2_source_v2':
				text_sources.append(obs.obs_source_get_name(source))

		obs.source_list_release(sources)

		return text_sources

	# Set the source for the given type to the provided text
	def set_source_text(self, data_type, text):
		print('Setting text of "{}" source to "{}"'.format(data_type, text))

		# Get the source and its settings
		source = obs.obs_get_source_by_name(self.sources[data_type])
		source_settings = obs.obs_data_create()

		# Set the text itself
		obs.obs_data_set_string(source_settings, 'text', text)
		obs.obs_source_update(source, source_settings)

		# Release the settings and source objects
		obs.obs_data_release(source_settings)
		obs.obs_source_release(source)

	# Check if the incoming request has all the data we need and is for our user
	def validate_data(self, raw_data):
		if 'player' not in raw_data or 'steamid' not in raw_data['player'] or 'match_stats' not in raw_data['player']:
			print('Data doesn\'t contain the required info, ignoring incoming request')
			return None
		elif self.steam_id is None or self.steam_id == '':
			print('Steam ID not set, ignoring incoming request')
			return None
		elif raw_data['player']['steamid'] != self.steam_id:
			# Data is not for the user we're tracking, ignoring incoming request
			return None
		else:
			return raw_data['player']['match_stats']

	# Validate the data and check if any of the values have changed, if so, update the text of corresponding source
	def handle_data(self, raw_data):
		global data_types

		data = self.validate_data(raw_data)

		if data is not None:
			for data_type in data_types:
				value = str(data[data_type])

				if self.sources[data_type] is None or self.sources[data_type] == '':
					print('Source name for type {} not set, ignoring incoming request'.format(data_type))
				elif value != self.last_states[data_type]:
					# Save the new state for future requests and set the text of the source
					self.last_states[data_type] = value
					self.set_source_text(data_type, value)

# Called to get the description shown in the Scripts window
def script_description():
	return 'Stream KAD'

# Called to show the settings GUI
def script_properties():
	global data_types
	global stream_kad

	props = obs.obs_properties_create()

	# Set up text field for the Steam ID
	obs.obs_properties_add_text(props, 'steam_id', 'Steam ID', obs.OBS_TEXT_DEFAULT)

	# Set up dropdowns for each stat and fill it with source names
	list_props = []

	for data_type in data_types:
		list_prop = obs.obs_properties_add_list(props, '{}_source'.format(data_type), '{} source name'.format(data_type.capitalize()), obs.OBS_COMBO_TYPE_LIST, obs.OBS_COMBO_FORMAT_STRING)
		list_props.append(list_prop)

	stream_kad.set_source_lists(list_props)

	# Set up a refresh button for the dropdowns
	obs.obs_properties_add_button(props, 'button', 'Refresh lists of sources', lambda props,prop: True if set_source_lists(list_props) else True)

	return props

# Called to set default setting values
def script_defaults(settings):
	global data_types

	obs.obs_data_set_default_string(settings, 'steam_id', '')

	for data_type in data_types:
		obs.obs_data_set_default_string(settings, '{}_source'.format(data_type), '')

# Called on setting changes and once after script load
def script_update(settings):
	global data_types
	global stream_kad

	stream_kad.steam_id = obs.obs_data_get_string(settings, 'steam_id')

	for data_type in data_types:
		stream_kad.sources[data_type] = obs.obs_data_get_string(settings, '{}_source'.format(data_type))

# Called on script load
def script_load(settings):
	global server
	global stream_kad

	print('Initializing script')

	# Initialize our main class
	stream_kad = StreamKAD()

	# Keep trying to initialize our HTTP server until binding is succesful
	server_running = False

	while server_running is False:
		try:
			server = StreamKADServer(('', 6901), StreamKADRequestHandler)
		except OSError:
			print('Unable to bind, will retry in 0.1 seconds')
			time.sleep(0.1)
		else:
			server_running = True

	# Run the HTTP server in a different thread to be able to run it at the same time as the main OBS loop
	thread = threading.Thread(target=server.serve_forever, kwargs={'post_handler': stream_kad.handle_data})
	thread.daemon = True
	thread.start()

# Called on script unload
def script_unload():
	global data_types
	global server
	global stream_kad

	# Reset the source texts to an empty value (dash)
	for data_type in data_types:
		if stream_kad.sources[data_type] is not None and stream_kad.sources[data_type] != '':
			stream_kad.set_source_text(data_type, '-')

	# If the server is still running, shut it down
	if server is not None:
		server.server_close()
